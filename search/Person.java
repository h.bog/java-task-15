package search; 

public class Person {
	private String firstName;
	private String lastName;
	private String telephone;

	Person(){
		firstName = "";
		lastName = "";
		telephone = "";
	}

	public Person(String firstName, String lastName, String telephone) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.telephone = telephone;
	 }

	public String getFirstName() {
		return this.firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public String getTelephone() {
		return this.telephone;
	}

}