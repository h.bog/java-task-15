import search.*;
import java.util.ArrayList; 

public class UpgradedNameSearch {
	public static void main(String[] args){
		String searchWords = "";
		boolean gotResults = false;
		ArrayList<Person> contactList = new ArrayList<Person>();
		contactList.add(new Person("Rose", "Howells", "004740567890"));
		contactList.add(new Person("Abbie", "King", "004799786543"));
		contactList.add(new Person("Tommy", "Warner", "004799789934"));
		contactList.add(new Person("Imogen", "Bright", "004747665760"));
		contactList.add(new Person("Tom", "Warren", "004789786768"));

		if (args.length != 1) {	
			System.out.println("Please enter a search word, Hint: name or telephone-number.");
		} else {
			for (String elem : args){
                searchWords += elem + " ";
            }
            searchWords = searchWords.substring(0, searchWords.length()-1);
            System.out.println("You searched for: " + searchWords + "\n");
            System.out.println("Search results:");
            for (Person contact : contactList) {
            	if (contact.getFirstName().toLowerCase().contains(searchWords.toLowerCase()) 
            		|| contact.getLastName().toLowerCase().contains(searchWords.toLowerCase())
            		|| contact.getTelephone().contains(searchWords)){
            		System.out.println(contact.getFirstName() + " " + contact.getLastName() + "     TLF: " + contact.getTelephone());
            		gotResults = true;
            	}
			}	
			if (!gotResults){
				System.out.println("No contacts contains the search word.");
			}
        }
	}
}

